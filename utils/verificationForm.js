// 对name字段进行必填验证
export const rules = {
	name: {
		rules: [{
				required: true,
				errorMessage: '请输入姓名',
			},
			{
				minLength: 3,
				maxLength: 10,
				errorMessage: '姓名长度在 {minLength} 到 {maxLength} 个字符',
			}
		]
	},
	// 对phone字段进行必填验证
	phone: {
		rules: [{
			required: true,
			errorMessage: '请输入手机号',
		}, {
			validateFunction: function(rule, value, data, callback) {
				if (!(/^1[34578]\d{9}$/.test(value))) {
					callback('手机号码有误，请重填')
				}
				return true
			}
		}]
	},
	id: {
		rules: [{
				required: true,
				errorMessage: '请输入身份证号码',
			},
			{
				validateFunction: function(rule, value, data, callback) {
					var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
					if (!(reg.test(value))) {
						callback('身份证号码输入有误，请重新输入')
					}
					return true
				}
				
			}
		]
	}
}
